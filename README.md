# NuCamp Bootstrap

This is the first course in NuCamp boot-camp It was created with using HTML, CSS, BOOTSTRAP. I can see why some want to just use HTML CSS and bootstrap. Bootstrap really does help with arranging your CSS.

This website is about different campground that to can reserve and Explore different Camp Grounds.

## This is using a json-server to retrieave data.
### json-server -H 0.0.0.0 --watch db.json -p 3001 -d 2000

## The first page is using bootstrap with Carousel.
![Home Page](/ScreenShots/homepage.png)


## The About Page
![About Page](/ScreenShots/aboutpage.png)

## The Contact Page. The page uses form section of html and the form group of bootstrap
![Contact Page](/ScreenShots/contactpage.png)

## The Reserve is using BootStrap modal to be able to reserve your camp site.
![Reserve Modal](/ScreenShots/reservemodal.png)

## The Log in section is using Bootstrap Modal.
![Log In Page](/ScreenShots/loginmodal.png)
